package controllers

import play.api.mvc._
import scala.concurrent._
import models.Tags
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import com.mongodb.casbah.Imports._

object TagsController extends Controller {
  def tags() = Action.async {
    val futureTags = Future {
      try {
        Tags asJson Tags.allTags
      } catch {
        case e: MongoException => throw e
      }
    }

    futureTags.map {
      tags => Ok(tags).as("application/json")
    }.recover {
      case e: MongoException => InternalServerError("{error: 'DB Error: "+e.getMessage+"'}").as("application/json")
    }
  }
}

package controllers

import play.api.mvc._

/**
 * playRSS entry point
 */
object Application extends Controller {

  /**
   * Main page. So it begins...
   * @return
   */
  def index = Action {
    Ok(views.html.index())
  }

}
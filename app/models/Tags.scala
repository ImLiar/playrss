package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json

/**
 * News tag container
 * @param name
 * @param total
 */
case class Tags(name: String, total: Int)

/**
 * Tags object allows to operate with tags in DB
 */
object Tags {

  /**
   * News collection contains all tag info
   */
  private val col: MongoCollection = Database.collection("news")

  /**
   * Get all tags as [{name: "", total: 0}] array of objects
   * @return
   */
  def allTags: Array[Tags] = {

    val group = MongoDBObject("$group" -> MongoDBObject(
      "_id" -> "$tags",
      "total" -> MongoDBObject("$sum" -> 1)
    ))

    val sort = MongoDBObject("$sort" -> MongoDBObject("total"-> -1))

    try {
      col.aggregate(group,sort).results.map((o: DBObject) => {
        val name = o.as[MongoDBList]("_id").toSeq.mkString(", ")
        val total = o.as[Int]("total")
        Tags(name, total)
      }).toArray
    } catch {
      case e: MongoException => throw e
    }
  }

  implicit def tagsWrites = Json.writes[Tags]

  /**
   * Converts tag array to JSON string
   * @param src
   * @return
   */
  def asJson(src: Array[Tags]) = {
    Json.stringify(Json.toJson(src))
  }
}

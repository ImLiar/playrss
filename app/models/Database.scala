package models

import com.mongodb.casbah.Imports._
import play.api.Play

/**
 * Simple object for DB connection
 */
object Database {
  private val db = MongoClient(
      Play.current.configuration.getString("mongo.host").get,
      Play.current.configuration.getInt("mongo.port").get).
    getDB(Play.current.configuration.getString("mongo.db").get)

  /**
   * Get collection by its name
   * @param collectionName
   * @return
   */
  def collection(collectionName:String) = db(collectionName)

  /**
   * Clear collection by its name
   * @param collectionName
   * @return
   */
  def clearCollection(collectionName:String) = db(collectionName).remove(MongoDBObject())

}
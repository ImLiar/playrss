define ["app/NewsModule"], (newsModule)->
  newsModule.controller "PanelCtrl", ["$scope", "$http", "broadcastService", ($scope, $http, broadcastService)->
    $scope.tags = []

    $http.get("/tags").success (data)->
      if(data.length)
        $scope.tags = data

    $scope.parseInProgress = false
    $scope.startParse = ()->
      if($scope.parseInProgress) then return
      $scope.parseInProgress = true
      broadcastService.broadcast "newParse", 1

    $scope.loadByTag = (tag) ->
      if tag.active
        tag.active = false
        broadcastService.broadcast("loadAll",0)
      else
        broadcastService.broadcast("loadByTag",tag.name)

    $scope.$on "selectTag", ()->
      name = broadcastService.message["selectTag"]
      (t.active = if t.name == name then true else false) for t in $scope.tags

    $scope.$on "parseDone", ()->
      $scope.parseInProgress = false

  ]
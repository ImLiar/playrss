define ["angular","ngInfinite"],(angular,infiniteScroll) ->
  newsModule = angular.module("News", ['infinite-scroll'])
  newsModule.factory 'broadcastService', ["$rootScope", ($rootScope) ->
    broadcastService =
      message: {},
      broadcast: (sub, msg)->
        if typeof msg == "number" then msg = {}
        this.message[sub] = angular.copy msg
        $rootScope.$broadcast(sub)
  ]
  newsModule


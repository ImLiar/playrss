require.config
  baseUrl: '/assets/javascripts',
  paths: {
    'angular': '//ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min',
    'jQuery': '//code.jquery.com/jquery.min'
    'ngInfinite': 'ng-infinite-scroll.min'
  },
  shim: {
    'jQuery': {'exports': 'jQuery'}
    'angular' : {'exports' : 'angular', deps:['jQuery']},
    'ngInfinite': {'exports': 'ngInfinite', deps: ['jQuery']}
  }

require ['jQuery','angular', 'app/bootstrap'] , (jQuery,angular, appBootstrap) ->
  $( ()-> angular.bootstrap(document,['News']) )
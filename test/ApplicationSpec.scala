import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.FakeApplication
import play.api.test.Helpers._
import models.Database

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication(fake) {
      route(FakeRequest(GET, "/boum")) must beNone
    }

    "render the index page" in new WithApplication(fake) {
      val home = route(FakeRequest(GET, "/")).get

      status(home) must equalTo(OK)
      contentType(home) must beSome.which(_ == "text/html")
      contentAsString(home) must contain("playRSS")

    }

    "get no news from fresh test DB" in new WithApplication(fake) {
      Database.clearCollection("news")
      val news = route(FakeRequest(GET, "/news")).get
      contentType(news) must beSome.which(_ == "application/json")
      contentAsString(news) must contain("[]")
    }

    "get no tags from fresh test DB" in new WithApplication(fake) {
      val tags = route(FakeRequest(GET, "/tags")).get
      contentType(tags) must beSome.which(_ == "application/json")
      contentAsString(tags) must contain("[]")
    }

    "get first 10 news from parsing of test xml" in new WithApplication(fake) {
      val p = route(FakeRequest(GET, "/parse")).get
      contentType(p) must beSome.which(_ == "application/json")
      contentAsString(p) must contain("1381482840") //last news timestamp
      contentAsString(p) must contain("1381484520") //first news timestamp
    }

    "get next 10 news from more request" in new WithApplication(fake) {
      val p = route(FakeRequest(GET, "/news?pubDate=1381482840")).get
      contentType(p) must beSome.which(_ == "application/json")
      contentAsString(p) must contain("1381482780")
      contentAsString(p) must contain("1381480260")
    }

    "get tags from database" in new WithApplication(fake) {
      val p = route(FakeRequest(GET, "/tags")).get
      contentType(p) must beSome.which(_ == "application/json")
      contentAsString(p) must contain("\"total\":6")
      contentAsString(p) must contain("\"total\":1")
    }

  }

  def fake = FakeApplication(additionalConfiguration = Map("mongo.db" -> "prss_test","rss.urls" -> List("test/misc/rss.xml")))
}
